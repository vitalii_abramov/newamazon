package com.newamazon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main class which represents entry point of project services module.
 *
 * @author Vitalii Abramov
 * @see SpringBootApplication
 */
@SpringBootApplication
public class Application {

    /**
     * Main method which launch project services module via Spring Boot.
     *
     * @param args starting arguments for application
     * @see SpringApplication
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
